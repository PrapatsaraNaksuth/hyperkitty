# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-12-21 18:59+0530\n"
"PO-Revision-Date: 2020-02-06 10:12+0000\n"
"Last-Translator: Paco <pacoc@pangea.org>\n"
"Language-Team: Catalan <https://hosted.weblate.org/projects/gnu-mailman/"
"hyperkitty/ca/>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 3.11-dev\n"

#: forms.py:64
msgid "Attach a file"
msgstr "Adjuntar un fitxer"

#: forms.py:65
msgid "Attach another file"
msgstr "Adjuntar un altre fitxer"

#: forms.py:66
msgid "Remove this file"
msgstr "Eliminar aquest arxiu"

#: templates/hyperkitty/404.html:28
msgid "Error 404"
msgstr "Error 404"

#: templates/hyperkitty/404.html:30 templates/hyperkitty/500.html:31
msgid "Oh No!"
msgstr "Oh No!"

#: templates/hyperkitty/404.html:32
msgid "I can't find this page."
msgstr "No puc trobar aquesta pàgina."

#: templates/hyperkitty/404.html:33 templates/hyperkitty/500.html:34
msgid "Go back home"
msgstr "Tornar a l'inici"

#: templates/hyperkitty/500.html:29
msgid "Error 500"
msgstr "Error 500"

#: templates/hyperkitty/500.html:33
msgid "Sorry, but the requested page is unavailable due to a server hiccup."
msgstr "El servidor ha tingut un problema i aquesta pàgina no està disponible."

#: templates/hyperkitty/ajax/reattach_suggest.html:7
#: templates/hyperkitty/reattach.html:25
msgid "started"
msgstr "iniciat"

#: templates/hyperkitty/ajax/reattach_suggest.html:7
#: templates/hyperkitty/reattach.html:25
msgid "last active:"
msgstr "últim actiu:"

#: templates/hyperkitty/ajax/reattach_suggest.html:8
msgid "see this thread"
msgstr "veure aquest fil"

#: templates/hyperkitty/ajax/reattach_suggest.html:12
msgid "(no suggestions)"
msgstr "(Cap suggeriment)"

#: templates/hyperkitty/ajax/temp_message.html:11
msgid "Sent just now, not yet distributed"
msgstr "Enviat ara mateix, encara no s'ha distribuït"

#: templates/hyperkitty/api.html:5
msgid "REST API"
msgstr "API REST"

#: templates/hyperkitty/api.html:7
msgid ""
"HyperKitty comes with a small REST API allowing you to programatically "
"retrieve emails and information."
msgstr ""
"HyperKitty inclou una API REST que li permet recuperar missatges i "
"informació per programa."

#: templates/hyperkitty/api.html:10
msgid "Formats"
msgstr "Formats"

#: templates/hyperkitty/api.html:12
msgid ""
"This REST API can return the information into several formats.  The default "
"format is html to allow human readibility."
msgstr ""
"Aquesta API REST pot retornar la informació en diversos formats. El defecte "
"és html per permetre la lectura per persones."

#: templates/hyperkitty/api.html:14
msgid ""
"To change the format, just add <em>?format=&lt;FORMAT&gt;</em> to the URL."
msgstr ""
"Per canviar el format, només cal afegir <em>?format=&lt;FORMAT&gt;</em> a "
"l’URL."

#: templates/hyperkitty/api.html:16
msgid "The list of available formats is:"
msgstr "La llista de formats disponibles és:"

#: templates/hyperkitty/api.html:20
msgid "Plain text"
msgstr "Text sense format"

#: templates/hyperkitty/api.html:26
msgid "List of mailing-lists"
msgstr "Llista de llistes de correu"

#: templates/hyperkitty/api.html:27 templates/hyperkitty/api.html:33
#: templates/hyperkitty/api.html:39 templates/hyperkitty/api.html:45
#: templates/hyperkitty/api.html:51
msgid "Endpoint:"
msgstr "Punt final:"

#: templates/hyperkitty/api.html:29
msgid ""
"Using this address you will be able to retrieve the information known about "
"all the mailing lists."
msgstr ""
"Amb aquesta adreça podreu recuperar la informació coneguda sobre totes les "
"llistes de distribució."

#: templates/hyperkitty/api.html:32
msgid "Threads in a mailing list"
msgstr "Fils en una llista de correu"

#: templates/hyperkitty/api.html:35
msgid ""
"Using this address you will be able to retrieve information about all the "
"threads on the specified mailing list."
msgstr ""
"Amb aquesta adreça podreu recuperar informació sobre tots els fils de la "
"llista de correu especificada."

#: templates/hyperkitty/api.html:38
msgid "Emails in a thread"
msgstr "Missatges de correu en un fil"

#: templates/hyperkitty/api.html:41
msgid ""
"Using this address you will be able to retrieve the list of emails in a "
"mailing list thread."
msgstr ""
"Amb aquesta adreça, podreu recuperar la llista de correus en un fil de "
"llista de correu."

#: templates/hyperkitty/api.html:44
msgid "An email in a mailing list"
msgstr "Un missatge d'una llista de correu"

#: templates/hyperkitty/api.html:47
msgid ""
"Using this address you will be able to retrieve the information known about "
"a specific email on the specified mailing list."
msgstr ""
"Amb aquesta adreça podreu recuperar la informació que es coneix sobre un "
"correu específic a la llista de correu especificada."

#: templates/hyperkitty/api.html:50
msgid "Tags"
msgstr "Etiquetes"

#: templates/hyperkitty/api.html:53
msgid "Using this address you will be able to retrieve the list of tags."
msgstr "Amb aquesta adreça podreu recuperar la llista d’etiquetes."

#: templates/hyperkitty/base.html:56 templates/hyperkitty/base.html:111
msgid "Account"
msgstr "Compte"

#: templates/hyperkitty/base.html:61 templates/hyperkitty/base.html:116
msgid "Mailman settings"
msgstr "Configuració de Mailman"

#: templates/hyperkitty/base.html:66 templates/hyperkitty/base.html:121
#: templates/hyperkitty/user_profile/base.html:17
msgid "Posting activity"
msgstr "Activitat d'enviaments"

#: templates/hyperkitty/base.html:71 templates/hyperkitty/base.html:126
msgid "Logout"
msgstr "Surt"

#: templates/hyperkitty/base.html:77 templates/hyperkitty/base.html:133
msgid "Sign In"
msgstr "Registra't"

#: templates/hyperkitty/base.html:81 templates/hyperkitty/base.html:137
msgid "Sign Up"
msgstr "Inscriure's"

#: templates/hyperkitty/base.html:90
msgid "Search this list"
msgstr "Cercar en aquesta llista"

#: templates/hyperkitty/base.html:90
#, fuzzy
#| msgid "Search this list"
msgid "Search all lists"
msgstr "Cercar en aquesta llista"

#: templates/hyperkitty/base.html:148
msgid "Manage this list"
msgstr "Administrar aquesta llista"

#: templates/hyperkitty/base.html:153
msgid "Manage lists"
msgstr "Gestiona les llistes"

#: templates/hyperkitty/base.html:191
msgid "Keyboard Shortcuts"
msgstr ""

#: templates/hyperkitty/base.html:194
#, fuzzy
#| msgid "Thread"
msgid "Thread View"
msgstr "Fil"

#: templates/hyperkitty/base.html:196
#, fuzzy
#| msgid "Delete message(s)"
msgid "Next unread message"
msgstr "Suprimir missatge(s)"

#: templates/hyperkitty/base.html:197
msgid "Previous unread message"
msgstr ""

#: templates/hyperkitty/base.html:198
#, fuzzy
#| msgid "All Threads"
msgid "Jump to all threads"
msgstr "Tots els fils"

#: templates/hyperkitty/base.html:199
#, fuzzy
#| msgid "List overview"
msgid "Jump to MailingList overview"
msgstr "Visió general de llista"

#: templates/hyperkitty/base.html:213
msgid "Powered by"
msgstr "Fa servir"

#: templates/hyperkitty/base.html:213
msgid "version"
msgstr "versió"

#: templates/hyperkitty/errors/notimplemented.html:7
msgid "Not implemented yet"
msgstr "Encara no implementat"

#: templates/hyperkitty/errors/notimplemented.html:12
msgid "Not implemented"
msgstr "No implementat"

#: templates/hyperkitty/errors/notimplemented.html:14
msgid "This feature has not been implemented yet, sorry."
msgstr "Aquesta característica no s'ha implementat encara, ho sentim."

#: templates/hyperkitty/errors/private.html:7
msgid "Error: private list"
msgstr "Error: llista privada"

#: templates/hyperkitty/errors/private.html:19
msgid ""
"This mailing list is private. You must be subscribed to view the archives."
msgstr ""
"Aquesta llista de correu és privada. S'ha de subscriure per veure els arxius."

#: templates/hyperkitty/fragments/like_form.html:16
msgid "You like it (cancel)"
msgstr "T'agrada (cancel·lar)"

#: templates/hyperkitty/fragments/like_form.html:24
msgid "You dislike it (cancel)"
msgstr "No t'agrada (cancel·lar)"

#: templates/hyperkitty/fragments/like_form.html:27
#: templates/hyperkitty/fragments/like_form.html:31
msgid "You must be logged-in to vote."
msgstr "Per poder votar, heu d’estar connectat."

#: templates/hyperkitty/fragments/month_list.html:6
#, fuzzy
#| msgid "Thread"
msgid "Threads by"
msgstr "Fil"

#: templates/hyperkitty/fragments/month_list.html:6
#, fuzzy
#| msgid "This month"
msgid " month"
msgstr "Aquest mes"

#: templates/hyperkitty/fragments/overview_threads.html:12
msgid "New messages in this thread"
msgstr "Missatges nous en aquest fil"

#: templates/hyperkitty/fragments/overview_threads.html:37
#: templates/hyperkitty/fragments/thread_left_nav.html:18
#: templates/hyperkitty/overview.html:75
msgid "All Threads"
msgstr "Tots els fils"

#: templates/hyperkitty/fragments/overview_top_posters.html:18
msgid "See the profile"
msgstr "Veure el perfil"

#: templates/hyperkitty/fragments/overview_top_posters.html:24
msgid "posts"
msgstr "enviaments"

#: templates/hyperkitty/fragments/overview_top_posters.html:29
msgid "No posters this month (yet)."
msgstr "No hi ha enviaments aquest mes (encara)."

#: templates/hyperkitty/fragments/send_as.html:5
msgid "This message will be sent as:"
msgstr "Aquest missatge s'enviarà com:"

#: templates/hyperkitty/fragments/send_as.html:6
msgid "Change sender"
msgstr "Canviar remitent"

#: templates/hyperkitty/fragments/send_as.html:16
msgid "Link another address"
msgstr "Afegir una altra direcció"

#: templates/hyperkitty/fragments/send_as.html:20
msgid ""
"If you aren't a current list member, sending this message will subscribe you."
msgstr ""

#: templates/hyperkitty/fragments/thread_left_nav.html:11
msgid "List overview"
msgstr "Visió general de llista"

#: templates/hyperkitty/fragments/thread_left_nav.html:27 views/message.py:75
#: views/mlist.py:102 views/thread.py:167
msgid "Download"
msgstr "Descarregar"

#: templates/hyperkitty/fragments/thread_left_nav.html:30
msgid "Past 30 days"
msgstr "Últims 30 dies"

#: templates/hyperkitty/fragments/thread_left_nav.html:31
msgid "This month"
msgstr "Aquest mes"

#: templates/hyperkitty/fragments/thread_left_nav.html:34
msgid "Entire archive"
msgstr "Arxiu sencer"

#: templates/hyperkitty/index.html:9 templates/hyperkitty/index.html:63
msgid "Available lists"
msgstr "Llistes disponibles"

#: templates/hyperkitty/index.html:22 templates/hyperkitty/index.html:27
#: templates/hyperkitty/index.html:72
msgid "Most popular"
msgstr "Més populars"

#: templates/hyperkitty/index.html:26
msgid "Sort by number of recent participants"
msgstr "Ordena per nombre de participants recents"

#: templates/hyperkitty/index.html:32 templates/hyperkitty/index.html:37
#: templates/hyperkitty/index.html:75
msgid "Most active"
msgstr "Més actius"

#: templates/hyperkitty/index.html:36
msgid "Sort by number of recent discussions"
msgstr "Ordenar per quantitat de debats recents"

#: templates/hyperkitty/index.html:42 templates/hyperkitty/index.html:47
#: templates/hyperkitty/index.html:78
msgid "By name"
msgstr "Per nom"

#: templates/hyperkitty/index.html:46
msgid "Sort alphabetically"
msgstr "Ordenar alfabèticament"

#: templates/hyperkitty/index.html:52 templates/hyperkitty/index.html:57
#: templates/hyperkitty/index.html:81
msgid "Newest"
msgstr "Més noves"

#: templates/hyperkitty/index.html:56
msgid "Sort by list creation date"
msgstr "Ordenar per data de creació de la llista"

#: templates/hyperkitty/index.html:68
msgid "Sort by"
msgstr "Ordenar per"

#: templates/hyperkitty/index.html:91
msgid "Hide inactive"
msgstr "Amagar inactives"

#: templates/hyperkitty/index.html:92
msgid "Hide private"
msgstr "Amagar privades"

#: templates/hyperkitty/index.html:99
msgid "Find list"
msgstr "Trobar llista"

#: templates/hyperkitty/index.html:123 templates/hyperkitty/index.html:193
#: templates/hyperkitty/user_profile/last_views.html:34
#: templates/hyperkitty/user_profile/last_views.html:73
msgid "new"
msgstr "noves"

#: templates/hyperkitty/index.html:134 templates/hyperkitty/index.html:204
msgid "private"
msgstr "privat"

#: templates/hyperkitty/index.html:136 templates/hyperkitty/index.html:206
msgid "inactive"
msgstr "inactives"

#: templates/hyperkitty/index.html:142 templates/hyperkitty/index.html:232
#: templates/hyperkitty/overview.html:91 templates/hyperkitty/overview.html:108
#: templates/hyperkitty/overview.html:178
#: templates/hyperkitty/overview.html:185
#: templates/hyperkitty/overview.html:192
#: templates/hyperkitty/overview.html:201
#: templates/hyperkitty/overview.html:209 templates/hyperkitty/reattach.html:39
#: templates/hyperkitty/thread.html:111
msgid "Loading..."
msgstr "Carregant..."

#: templates/hyperkitty/index.html:148 templates/hyperkitty/index.html:221
#: templates/hyperkitty/overview.html:100
#: templates/hyperkitty/thread_list.html:36
#: templates/hyperkitty/threads/right_col.html:44
#: templates/hyperkitty/threads/right_col.html:97
#: templates/hyperkitty/threads/summary_thread_large.html:47
msgid "participants"
msgstr "participants"

#: templates/hyperkitty/index.html:153 templates/hyperkitty/index.html:226
#: templates/hyperkitty/overview.html:101
#: templates/hyperkitty/thread_list.html:41
msgid "discussions"
msgstr "debats"

#: templates/hyperkitty/index.html:162 templates/hyperkitty/index.html:240
msgid "No archived list yet."
msgstr "Cap llista arxivada encara."

#: templates/hyperkitty/index.html:174
#: templates/hyperkitty/user_profile/favorites.html:40
#: templates/hyperkitty/user_profile/last_views.html:45
#: templates/hyperkitty/user_profile/profile.html:15
#: templates/hyperkitty/user_profile/subscriptions.html:41
#: templates/hyperkitty/user_profile/votes.html:46
msgid "List"
msgstr "Llista"

#: templates/hyperkitty/index.html:175
msgid "Description"
msgstr "Descripció"

#: templates/hyperkitty/index.html:176
msgid "Activity in the past 30 days"
msgstr "Activitat en els últims 30 dies"

#: templates/hyperkitty/list_delete.html:7
msgid "Delete MailingList"
msgstr ""

#: templates/hyperkitty/list_delete.html:20
msgid "Delete Mailing List"
msgstr ""

#: templates/hyperkitty/list_delete.html:26
msgid ""
"will be deleted along with all the threads and messages. Do you want to "
"continue?"
msgstr ""

#: templates/hyperkitty/list_delete.html:33
#: templates/hyperkitty/message_delete.html:44
#: templates/hyperkitty/overview.html:78
msgid "Delete"
msgstr "Suprimir"

#: templates/hyperkitty/list_delete.html:34
#: templates/hyperkitty/message_delete.html:45
#: templates/hyperkitty/message_new.html:53
#: templates/hyperkitty/messages/message.html:146
msgid "or"
msgstr "o"

#: templates/hyperkitty/list_delete.html:36
#: templates/hyperkitty/message_delete.html:45
#: templates/hyperkitty/message_new.html:53
#: templates/hyperkitty/messages/message.html:146
#: templates/hyperkitty/user_profile/votes.html:36
#: templates/hyperkitty/user_profile/votes.html:74
msgid "cancel"
msgstr "cancel·lar"

#: templates/hyperkitty/message.html:22
msgid "thread"
msgstr "fil"

#: templates/hyperkitty/message_delete.html:7
#: templates/hyperkitty/message_delete.html:20
msgid "Delete message(s)"
msgstr "Suprimir missatge(s)"

#: templates/hyperkitty/message_delete.html:25
#, python-format
msgid ""
"\n"
"        %(count)s message(s) will be deleted. Do you want to continue?\n"
"        "
msgstr ""
"\n"
"        S'eliminarà(n) %(count)s missatge(s). Voleu continuar?\n"
"        "

#: templates/hyperkitty/message_new.html:8
#: templates/hyperkitty/message_new.html:21
msgid "Create a new thread"
msgstr "Crear un nou fil"

#: templates/hyperkitty/message_new.html:22
#: templates/hyperkitty/user_posts.html:22
msgid "in"
msgstr "en"

#: templates/hyperkitty/message_new.html:52
#: templates/hyperkitty/messages/message.html:145
msgid "Send"
msgstr "Enviar"

#: templates/hyperkitty/messages/message.html:17
#, python-format
msgid "See the profile for %(name)s"
msgstr "Mostra el perfil de %(name)s"

#: templates/hyperkitty/messages/message.html:27
msgid "Unread"
msgstr "Sense llegir"

#: templates/hyperkitty/messages/message.html:44
msgid "Sender's time:"
msgstr "Hora del remitent:"

#: templates/hyperkitty/messages/message.html:50
msgid "New subject:"
msgstr "Assumpte nou:"

#: templates/hyperkitty/messages/message.html:61
msgid "Attachments:"
msgstr "Arxius adjunts:"

#: templates/hyperkitty/messages/message.html:76
msgid "Display in fixed font"
msgstr "Visualització en tipus de lletra fixa"

#: templates/hyperkitty/messages/message.html:79
msgid "Permalink for this message"
msgstr "Enllaç permanent per a aquest missatge"

#: templates/hyperkitty/messages/message.html:90
#: templates/hyperkitty/messages/message.html:96
msgid "Reply"
msgstr "Respondre"

#: templates/hyperkitty/messages/message.html:93
msgid "Sign in to reply online"
msgstr "Iniciar la sessió per respondre en línia"

#: templates/hyperkitty/messages/message.html:105
#, python-format
msgid ""
"\n"
"                %(email.attachments.count)s attachment\n"
"                "
msgid_plural ""
"\n"
"                %(email.attachments.count)s attachments\n"
"                "
msgstr[0] ""
"\n"
"                %(email.attachments.count)s adjunt\n"
"                "
msgstr[1] ""
"\n"
"                %(email.attachments.count)s adjunts\n"
"                "

#: templates/hyperkitty/messages/message.html:131
msgid "Quote"
msgstr "Citar"

#: templates/hyperkitty/messages/message.html:132
msgid "Create new thread"
msgstr "Crear nou fil"

#: templates/hyperkitty/messages/message.html:135
msgid "Use email software"
msgstr "Utilitzar un programa de correu"

#: templates/hyperkitty/messages/right_col.html:11
msgid "Back to the thread"
msgstr "Tornar al fil"

#: templates/hyperkitty/messages/right_col.html:18
msgid "Back to the list"
msgstr "Tornar a la llista"

#: templates/hyperkitty/messages/right_col.html:27
msgid "Delete this message"
msgstr "Suprimir aquest missatge"

#: templates/hyperkitty/messages/summary_message.html:23
#, python-format
msgid ""
"\n"
"                                by %(name)s\n"
"                            "
msgstr ""
"\n"
"                                per %(name)s\n"
"                            "

#: templates/hyperkitty/overview.html:35
msgid "Home"
msgstr "Inici"

#: templates/hyperkitty/overview.html:38 templates/hyperkitty/thread.html:78
msgid "Stats"
msgstr "Estadístiques"

#: templates/hyperkitty/overview.html:41
#, fuzzy
#| msgid "Thread"
msgid "Threads"
msgstr "Fil"

#: templates/hyperkitty/overview.html:47 templates/hyperkitty/overview.html:58
#: templates/hyperkitty/thread_list.html:44
msgid "You must be logged-in to create a thread."
msgstr "Per crear un fil, heu d’iniciar sessió."

#: templates/hyperkitty/overview.html:60
#: templates/hyperkitty/thread_list.html:48
#, fuzzy
#| msgid ""
#| "<span class=\"hidden-tn hidden-xs\">Start a </span><span class=\"hidden-"
#| "sm hidden-md hidden-lg\">N</span>ew thread"
msgid ""
"<span class=\"d-none d-md-inline\">Start a n</span><span class=\"d-md-none"
"\">N</span>ew thread"
msgstr ""
"<span class=\"hidden-tn hidden-xs\"> Iniciar un </span><span class=\"hidden-"
"sm hidden-md hidden-lg\">N</span>ou fil"

#: templates/hyperkitty/overview.html:72
#, fuzzy
#| msgid ""
#| "<span class=\"hidden-tn hidden-xs\">Manage s</span><span class=\"hidden-"
#| "sm hidden-md hidden-lg\">S</span>ubscription"
msgid ""
"<span class=\"d-none d-md-inline\">Manage s</span><span class=\"d-md-none"
"\">S</span>ubscription"
msgstr ""
"<span class = \"hidden-tn hidden-xs\">Gestiona </span> <span class = "
"\"hidden-sm hidden-md hidden-lg\">S</span>ubscripció"

#: templates/hyperkitty/overview.html:88
msgid "Activity Summary"
msgstr "Resum d'activitat"

#: templates/hyperkitty/overview.html:90
msgid "Post volume over the past <strong>30</strong> days."
msgstr "Volum d'enviaments durant els últims <strong>30</strong> dies."

#: templates/hyperkitty/overview.html:95
msgid "The following statistics are from"
msgstr "Les següents estadístiques són de"

#: templates/hyperkitty/overview.html:96
msgid "In"
msgstr "En"

#: templates/hyperkitty/overview.html:97
msgid "the past <strong>30</strong> days:"
msgstr "els darrers <strong>30</strong> dies:"

#: templates/hyperkitty/overview.html:106
msgid "Most active posters"
msgstr "Participants més actius"

#: templates/hyperkitty/overview.html:115
msgid "Prominent posters"
msgstr "Participants destacats"

#: templates/hyperkitty/overview.html:130
msgid "kudos"
msgstr "reconeixement"

#: templates/hyperkitty/overview.html:176
msgid "Recently active discussions"
msgstr "Debats actius recents"

#: templates/hyperkitty/overview.html:183
msgid "Most popular discussions"
msgstr "Debats més populars"

#: templates/hyperkitty/overview.html:190
msgid "Most active discussions"
msgstr "Debats més actius"

#: templates/hyperkitty/overview.html:197
msgid "Discussions You've Flagged"
msgstr "Debats que heu marcat"

#: templates/hyperkitty/overview.html:205
msgid "Discussions You've Posted to"
msgstr "Debats en que heu enviat a"

#: templates/hyperkitty/reattach.html:9
msgid "Reattach a thread"
msgstr "Torna a adjuntar un fil"

#: templates/hyperkitty/reattach.html:20
msgid "Re-attach a thread to another"
msgstr "Tornar a adjuntar un fil a un altre"

#: templates/hyperkitty/reattach.html:22
msgid "Thread to re-attach:"
msgstr "Fil per tornar a adjuntar:"

#: templates/hyperkitty/reattach.html:29
msgid "Re-attach it to:"
msgstr "Torneu-lo a adjuntar a:"

#: templates/hyperkitty/reattach.html:31
msgid "Search for the parent thread"
msgstr "Cercar el fil pare"

#: templates/hyperkitty/reattach.html:32
msgid "Search"
msgstr "Cerca"

#: templates/hyperkitty/reattach.html:44
msgid "this thread ID:"
msgstr "aquest ID de fil:"

#: templates/hyperkitty/reattach.html:50
msgid "Do it"
msgstr "Fer-ho"

#: templates/hyperkitty/reattach.html:50
msgid "(there's no undoing!), or"
msgstr "(No es pot desfer), o"

#: templates/hyperkitty/reattach.html:52
msgid "go back to the thread"
msgstr "tornar al fil"

#: templates/hyperkitty/search_results.html:8
msgid "Search results for"
msgstr "Resultats de cerca per a"

#: templates/hyperkitty/search_results.html:30
msgid "search results"
msgstr "resultats de la cerca"

#: templates/hyperkitty/search_results.html:32
msgid "Search results"
msgstr "Resultats de la cerca"

#: templates/hyperkitty/search_results.html:34
msgid "for query"
msgstr "per a la consulta"

#: templates/hyperkitty/search_results.html:44
#: templates/hyperkitty/threads/right_col.html:49
#: templates/hyperkitty/user_posts.html:34
msgid "messages"
msgstr "missatges"

#: templates/hyperkitty/search_results.html:57
msgid "sort by score"
msgstr "ordenar per puntuació"

#: templates/hyperkitty/search_results.html:60
msgid "sort by latest first"
msgstr "ordenar per últim primer"

#: templates/hyperkitty/search_results.html:63
msgid "sort by earliest first"
msgstr "ordena segons el més primerenc"

#: templates/hyperkitty/search_results.html:84
msgid "Sorry no email could be found for this query."
msgstr "No s'ha trobat cap missatge per a aquesta consulta."

#: templates/hyperkitty/search_results.html:87
msgid "Sorry but your query looks empty."
msgstr "La seva consulta sembla buida."

#: templates/hyperkitty/search_results.html:88
msgid "these are not the messages you are looking for"
msgstr "aquests no són els missatges que està buscant"

#: templates/hyperkitty/thread.html:30
msgid "newer"
msgstr "més recent"

#: templates/hyperkitty/thread.html:44
msgid "older"
msgstr "més vells"

#: templates/hyperkitty/thread.html:72
msgid "First Post"
msgstr "Primer Enviament"

#: templates/hyperkitty/thread.html:75
#: templates/hyperkitty/user_profile/favorites.html:45
#: templates/hyperkitty/user_profile/last_views.html:50
msgid "Replies"
msgstr "Respostes"

#: templates/hyperkitty/thread.html:97
msgid "Show replies by thread"
msgstr "Mostra respostes per fil"

#: templates/hyperkitty/thread.html:100
msgid "Show replies by date"
msgstr "Mostrar respostes per data"

#: templates/hyperkitty/thread_list.html:56
msgid "Sorry no email threads could be found"
msgstr "No s'ha trobat cap fil de correu"

#: templates/hyperkitty/threads/category.html:7
msgid "Click to edit"
msgstr "Fer clic per editar"

#: templates/hyperkitty/threads/category.html:9
msgid "You must be logged-in to edit."
msgstr "Heu d'haver iniciat la sessió per editar-la."

#: templates/hyperkitty/threads/category.html:15
msgid "no category"
msgstr "sense categoria"

#: templates/hyperkitty/threads/right_col.html:12
msgid "days inactive"
msgstr "dies inactiu"

#: templates/hyperkitty/threads/right_col.html:18
msgid "days old"
msgstr "dies"

#: templates/hyperkitty/threads/right_col.html:40
#: templates/hyperkitty/threads/summary_thread_large.html:52
msgid "comments"
msgstr "comentaris"

#: templates/hyperkitty/threads/right_col.html:48
msgid "unread"
msgstr "no llegits"

#: templates/hyperkitty/threads/right_col.html:59
msgid "You must be logged-in to have favorites."
msgstr "Per tenir favorits, heu d’estar connectat."

#: templates/hyperkitty/threads/right_col.html:60
msgid "Add to favorites"
msgstr "Afegir a preferits"

#: templates/hyperkitty/threads/right_col.html:62
msgid "Remove from favorites"
msgstr "Eliminar de Preferits"

#: templates/hyperkitty/threads/right_col.html:71
msgid "Reattach this thread"
msgstr "Torna a adjuntar aquest fil"

#: templates/hyperkitty/threads/right_col.html:75
msgid "Delete this thread"
msgstr "Suprimeix aquest fil"

#: templates/hyperkitty/threads/right_col.html:113
msgid "Unreads:"
msgstr "Sense llegir:"

#: templates/hyperkitty/threads/right_col.html:115
msgid "Go to:"
msgstr "Anar a:"

#: templates/hyperkitty/threads/right_col.html:115
msgid "next"
msgstr "següent"

#: templates/hyperkitty/threads/right_col.html:116
msgid "prev"
msgstr "anterior"

#: templates/hyperkitty/threads/summary_thread_large.html:21
#: templates/hyperkitty/threads/summary_thread_large.html:23
msgid "Favorite"
msgstr "Favorit"

#: templates/hyperkitty/threads/summary_thread_large.html:29
#, fuzzy, python-format
#| msgid ""
#| "\n"
#| "                                by %(name)s\n"
#| "                            "
msgid ""
"\n"
"                    by %(name)s\n"
"                    "
msgstr ""
"\n"
"                                per %(name)s\n"
"                            "

#: templates/hyperkitty/threads/summary_thread_large.html:39
msgid "Most recent thread activity"
msgstr "Activitat de fil més recent"

#: templates/hyperkitty/threads/tags.html:3
msgid "tags"
msgstr "etiquetes"

#: templates/hyperkitty/threads/tags.html:9
msgid "Search for tag"
msgstr "Cerca per l'etiqueta"

#: templates/hyperkitty/threads/tags.html:15
msgid "Remove"
msgstr "Suprimeix"

#: templates/hyperkitty/user_posts.html:8
#: templates/hyperkitty/user_posts.html:21
#: templates/hyperkitty/user_posts.html:25
msgid "Messages by"
msgstr "Missatges per"

#: templates/hyperkitty/user_posts.html:38
#, python-format
msgid "Back to %(fullname)s's profile"
msgstr "Tornar al perfil de %(fullname)s"

#: templates/hyperkitty/user_posts.html:48
msgid "Sorry no email could be found by this user."
msgstr "No s'ha trobat cap missatge d'aquest usuari."

#: templates/hyperkitty/user_profile/base.html:5
#: templates/hyperkitty/user_profile/base.html:12
msgid "User posting activity"
msgstr "Activitat d'enviament de l'usuari"

#: templates/hyperkitty/user_profile/base.html:12
#: templates/hyperkitty/user_public_profile.html:7
#: templates/hyperkitty/user_public_profile.html:14
msgid "for"
msgstr "per"

#: templates/hyperkitty/user_profile/base.html:22
msgid "Favorites"
msgstr "Favorits"

#: templates/hyperkitty/user_profile/base.html:26
msgid "Threads you have read"
msgstr "Fils que heu llegit"

#: templates/hyperkitty/user_profile/base.html:30
#: templates/hyperkitty/user_profile/profile.html:18
#: templates/hyperkitty/user_profile/subscriptions.html:45
msgid "Votes"
msgstr "Vots"

#: templates/hyperkitty/user_profile/base.html:34
msgid "Subscriptions"
msgstr "Subscripcions"

#: templates/hyperkitty/user_profile/favorites.html:24
#: templates/hyperkitty/user_profile/last_views.html:27
#: templates/hyperkitty/user_profile/votes.html:23
msgid "Original author:"
msgstr "Autor original:"

#: templates/hyperkitty/user_profile/favorites.html:26
#: templates/hyperkitty/user_profile/last_views.html:29
#: templates/hyperkitty/user_profile/votes.html:25
msgid "Started on:"
msgstr "Inici:"

#: templates/hyperkitty/user_profile/favorites.html:28
#: templates/hyperkitty/user_profile/last_views.html:31
msgid "Last activity:"
msgstr "Darrera activitat:"

#: templates/hyperkitty/user_profile/favorites.html:30
#: templates/hyperkitty/user_profile/last_views.html:33
msgid "Replies:"
msgstr "Respostes:"

#: templates/hyperkitty/user_profile/favorites.html:41
#: templates/hyperkitty/user_profile/last_views.html:46
#: templates/hyperkitty/user_profile/profile.html:16
#: templates/hyperkitty/user_profile/votes.html:47
msgid "Subject"
msgstr "Assumpte"

#: templates/hyperkitty/user_profile/favorites.html:42
#: templates/hyperkitty/user_profile/last_views.html:47
#: templates/hyperkitty/user_profile/votes.html:48
msgid "Original author"
msgstr "Autor original"

#: templates/hyperkitty/user_profile/favorites.html:43
#: templates/hyperkitty/user_profile/last_views.html:48
#: templates/hyperkitty/user_profile/votes.html:49
msgid "Start date"
msgstr "Data d'inici"

#: templates/hyperkitty/user_profile/favorites.html:44
#: templates/hyperkitty/user_profile/last_views.html:49
msgid "Last activity"
msgstr "Última activitat"

#: templates/hyperkitty/user_profile/favorites.html:71
msgid "No favorites yet."
msgstr "Cap favorit encara."

#: templates/hyperkitty/user_profile/last_views.html:22
#: templates/hyperkitty/user_profile/last_views.html:59
msgid "New comments"
msgstr "Nous comentaris"

#: templates/hyperkitty/user_profile/last_views.html:82
msgid "Nothing read yet."
msgstr "Res no llegeix encara."

#: templates/hyperkitty/user_profile/profile.html:9
msgid "Last posts"
msgstr "Últims enviaments"

#: templates/hyperkitty/user_profile/profile.html:17
msgid "Date"
msgstr "Data"

#: templates/hyperkitty/user_profile/profile.html:19
msgid "Thread"
msgstr "Fil"

#: templates/hyperkitty/user_profile/profile.html:20
msgid "Last thread activity"
msgstr "Última activitat del fil"

#: templates/hyperkitty/user_profile/profile.html:49
msgid "No posts yet."
msgstr "Encara no hi ha cap enviament."

#: templates/hyperkitty/user_profile/subscriptions.html:24
msgid "since first post"
msgstr "des del primer enviament"

#: templates/hyperkitty/user_profile/subscriptions.html:26
#: templates/hyperkitty/user_profile/subscriptions.html:63
msgid "post"
msgstr "enviament"

#: templates/hyperkitty/user_profile/subscriptions.html:31
#: templates/hyperkitty/user_profile/subscriptions.html:69
msgid "no post yet"
msgstr "encara no hi ha cap enviament"

#: templates/hyperkitty/user_profile/subscriptions.html:42
msgid "Time since the first activity"
msgstr "Temps des de la primera activitat"

#: templates/hyperkitty/user_profile/subscriptions.html:43
msgid "First post"
msgstr "Primer enviament"

#: templates/hyperkitty/user_profile/subscriptions.html:44
msgid "Posts to this list"
msgstr "Enviaments en aquesta llista"

#: templates/hyperkitty/user_profile/subscriptions.html:76
msgid "no subscriptions"
msgstr "no hi ha cap subscripció"

#: templates/hyperkitty/user_profile/votes.html:32
#: templates/hyperkitty/user_profile/votes.html:70
msgid "You like it"
msgstr "T'agrada"

#: templates/hyperkitty/user_profile/votes.html:34
#: templates/hyperkitty/user_profile/votes.html:72
msgid "You dislike it"
msgstr "No t'agrada"

#: templates/hyperkitty/user_profile/votes.html:50
msgid "Vote"
msgstr "Votar"

#: templates/hyperkitty/user_profile/votes.html:83
msgid "No vote yet."
msgstr "Cap vot encara."

#: templates/hyperkitty/user_public_profile.html:7
msgid "User Profile"
msgstr "Perfil d'usuari"

#: templates/hyperkitty/user_public_profile.html:14
msgid "User profile"
msgstr "Perfil d'usuari"

#: templates/hyperkitty/user_public_profile.html:23
msgid "Name:"
msgstr "Nom:"

#: templates/hyperkitty/user_public_profile.html:28
msgid "Creation:"
msgstr "Creació:"

#: templates/hyperkitty/user_public_profile.html:33
msgid "Votes for this user:"
msgstr "Vots per a aquest usuari:"

#: templates/hyperkitty/user_public_profile.html:41
msgid "Email addresses:"
msgstr "Adreces de correu:"

#: views/message.py:76
msgid "This message in gzipped mbox format"
msgstr "Aquest missatge en format mbox comprimit amb gzip"

#: views/message.py:200
msgid "Your reply has been sent and is being processed."
msgstr ""

#: views/message.py:204
#, fuzzy
#| msgid "You have not posted to this list (yet)."
msgid ""
"\n"
"  You have been subscribed to {} list."
msgstr "No heu publicat a aquesta llista (encara)."

#: views/message.py:287
#, python-format
msgid "Could not delete message %(msg_id_hash)s: %(error)s"
msgstr "No s'ha pogut suprimir el missatge %(msg_id_hash)s: %(error)s"

#: views/message.py:296
#, python-format
msgid "Successfully deleted %(count)s messages."
msgstr "Es van suprimit %(count)s missatges correctament."

#: views/mlist.py:88
msgid "for this month"
msgstr "per a aquest mes"

#: views/mlist.py:91
msgid "for this day"
msgstr "per a aquest dia"

#: views/mlist.py:103
msgid "This month in gzipped mbox format"
msgstr "Aquest mes en format mbox comprimit amb gzip"

#: views/mlist.py:200 views/mlist.py:224
msgid "No discussions this month (yet)."
msgstr "No hi ha debats aquest mes (encara)."

#: views/mlist.py:212
msgid "No vote has been cast this month (yet)."
msgstr "No s'ha emès cap vot aquest mes (encara)."

#: views/mlist.py:241
msgid "You have not flagged any discussions (yet)."
msgstr "No heu marcat cap debat (encara)."

#: views/mlist.py:264
msgid "You have not posted to this list (yet)."
msgstr "No heu publicat a aquesta llista (encara)."

#: views/mlist.py:353
msgid "You must be a staff member to delete a MailingList"
msgstr ""

#: views/mlist.py:367
#, fuzzy
#| msgid "Successfully deleted %(count)s messages."
msgid "Successfully deleted {}"
msgstr "Es van suprimit %(count)s missatges correctament."

#: views/search.py:115
#, python-format
msgid "Parsing error: %(error)s"
msgstr "Error de processament: %(error)s"

#: views/thread.py:168
msgid "This thread in gzipped mbox format"
msgstr "Aquest fil en format mbox comprimit amb gzip"

#~ msgid "Go to"
#~ msgstr "Anar a"

#~ msgid "More..."
#~ msgstr "Més..."

#~ msgid "Discussions"
#~ msgstr "Debats"

#~ msgid "most recent"
#~ msgstr "més recents"

#~ msgid "most popular"
#~ msgstr "més populars"

#~ msgid "most active"
#~ msgstr "més actius"

#~ msgid "Update"
#~ msgstr "Actualitzar"

#, python-format
#~ msgid ""
#~ "\n"
#~ "                                        by %(name)s\n"
#~ "                                    "
#~ msgstr ""
#~ "\n"
#~ "                                        per %(name)s\n"
#~ "                                    "
